﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
public class PhysicalButton : MonoBehaviour
{
    public UnityAction actionOnPress;
    public bool oneTimeAction = true;
    private bool hasDoneAction = false;

    public Transform movingPart;
    public Transform moveToPoint;
    Vector3 movingPartDefaultPosition;
  
    public void DoButtonAction()
    {
        StartCoroutine(MovePart());
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponentInParent<OVRTouchSample.Hand>() && !hasDoneAction)
        {
            DoButtonAction();

            if(oneTimeAction)
            hasDoneAction = true;
            
        }
    }

    IEnumerator MovePart()
    {
        movingPartDefaultPosition = movingPart.position;
        movingPart.position = moveToPoint.position;
        
        yield return new WaitForSeconds(1);
        movingPart.position = movingPartDefaultPosition;
    }
}
