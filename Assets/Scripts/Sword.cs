﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sword : MonoBehaviour,IHandItem
{
    public bool canCut = true;
    
    public Collider m_BladeCollider;
    
    public GameObject currentlyCuttingObject;
   public Vector3 positionCutStart, positionCutEnd;
    public bool activated = false;
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    private void OnTriggerEnter(Collider other)
    {
        
        if (!activated) return;
        if (other.CompareTag("Cuttable"))
        {
            Debug.Log("sword entered" + other.name);
            currentlyCuttingObject = other.gameObject;
            positionCutStart = m_BladeCollider.ClosestPointOnBounds(other.transform.position);

        }
    }

    private void OnTriggerExit(Collider other)
    {
        
        if (!activated) return;
        if(other.gameObject == currentlyCuttingObject)
        {
            Debug.Log("sword exited" + other.name);
            positionCutEnd = m_BladeCollider.ClosestPointOnBounds(other.transform.position);

            CreateSlicePlane();
            positionCutEnd = Vector3.zero;
            positionCutStart = Vector3.zero;
            currentlyCuttingObject = null;
        }
    }

    void CreateSlicePlane()
    {
        Vector3 centre = (positionCutStart + positionCutEnd) / 2;
        Vector3 up = Vector3.Cross((positionCutStart - positionCutEnd), (positionCutStart - transform.position)).normalized;
        
        Cutter.Cut(currentlyCuttingObject, centre, up, null, true, true);
    }

    public void ActivateHandItem()
    {
        activated = true;
    }

    public void DeactivateHandItem()
    {
        activated = false;
    }
}
