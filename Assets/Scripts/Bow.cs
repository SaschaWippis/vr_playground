﻿using System;
using System.Collections;
using UnityEngine;

public class Bow : MonoBehaviour
{
    [Header("Assets")]
    public GameObject m_ArrowPrefab;

    [Header("Bow")]
    public float m_GrabThreshold = 0.15f;
    public Transform m_Start;
    public Transform m_End;
    public Transform m_Socket;
    public Transform m_PullingHand;

    private Arrow m_CurrentArrow;
    private Animator m_Animator;
    private Rigidbody m_Rigidbody;

    private bool pickedUp = false;
    private float m_PullValue = 0.0f;
    private Collider m_Collider;
    private void Awake()
    {
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_Collider = GetComponent<Collider>();
    }

    private void Start()
    {
    }


    private void Update()
    {
       
        if (!m_CurrentArrow || !m_PullingHand)
            return;

        m_PullValue = CalculatePull(m_PullingHand);
        m_PullValue = Mathf.Clamp(m_PullValue, 0f, 1f);

        m_Animator.SetFloat("Blend", m_PullValue);
    }

    public Arrow getCurrentArrow()
    {
        return m_CurrentArrow;
    }

    private float CalculatePull(Transform pullHand)
    {
        Vector3 direction = m_End.position - m_Start.position;
        float magnitude = direction.magnitude;

        direction.Normalize();

        Vector3 difference = pullHand.position - m_Start.position;

        return Vector3.Dot(difference, direction) / magnitude;

    }

    public IEnumerator CreateArrow(float waitTime)
    {

        yield return new WaitForSeconds(waitTime);

        GameObject arrowObject = Instantiate(m_ArrowPrefab, m_Socket);

        arrowObject.transform.localPosition = new Vector3(0, 0, 0.425f);
        arrowObject.transform.localEulerAngles = Vector3.zero;

        m_CurrentArrow = arrowObject.GetComponent<Arrow>();

    }

    public void Pull(Transform hand)
    {
        float distance = Vector3.Distance(hand.position, m_Start.position);

        if(distance > m_GrabThreshold)
            return;

        m_PullingHand = hand;
    }

    public void Release()
    {
        if(m_PullValue > 0.25f)
           FireArrow();

        m_PullingHand = null;
        m_PullValue = 0;
        m_Animator.SetFloat("Blend", 0);
        
    }
    public void ResetBow()
    {
        Release();

    }

    public void FireArrow()
    {
        m_CurrentArrow.Fire(m_PullValue);

        VibrationManager.BuzzNode(.6f, VibrationForce.MEDIUM, OVRInput.Controller.LTouch);
        m_CurrentArrow = null;
    }

}
