﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour, IHandItem
{
    WeaponGrabbable m_Grabbable;
    [Header("Assets")]
    public GameObject m_BulletPrefab;
    public GameObject m_CasingPrefab;
    public GameObject m_MuzzleFlashPrefab;
    [Header("Parts")]
    public Transform m_BarrelLocation;
    public Transform m_CasingExitLocation;

    [Header("Ammo")]
    public Transform m_MagazineHolder;
    [HideInInspector]
    public Collider m_magazineHolderCollider;
    public Magazine m_MagazineUsed;
    public Magazine m_CurrentMagazine;
    public WeaponMagazine m_MagazineInHand;
    public bool animationForMagazineHolder;



    [Header("Shooting")]
    public VibrationForce m_shootVibrationForce = VibrationForce.HARD;
    public float m_ShootVibrationDuration = 0.4f;
    public float m_ShotPower = 200f;

    private Animator m_Animator;
    private void Awake()
    {
        m_Animator = GetComponent<Animator>();
        m_Grabbable = GetComponent<WeaponGrabbable>();
        m_magazineHolderCollider = m_MagazineHolder.GetComponent<Collider>();
    }

    void Start()
    {
        if (m_BarrelLocation == null)
            m_BarrelLocation = transform;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {

            if (m_Animator)
                m_Animator.SetTrigger("Fire");
            else
                Shoot();
        }

        if (!m_Grabbable.grabbedWithController)
        {
            return;
        }
        
        if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger, m_Grabbable.grabbedWithController.m_Controller))
        {
            DebugGrabOffset.instance.SetMagazineText(
            $"CurrentMagazine in: {m_CurrentMagazine.magazine != null}\n" +
            $"MagazineUsed has magazine: {m_MagazineUsed.magazine != null}\n"

            );
            if (m_MagazineUsed.magazine && m_CurrentMagazine.magazine)
            {

                if (m_Animator)
                    m_Animator.SetTrigger("Fire");
                else
                    Shoot();
            }
            else
            {
                Shoot();
            }
            
        }
        if (OVRInput.GetDown(OVRInput.Button.Two, m_Grabbable.grabbedWithController.m_Controller))
        {
            DebugGrabOffset.instance.SetMagazineText(
            $"CurrentMagazine in: {m_CurrentMagazine.magazine != null}\n" +
            $"MagazineUsed has magazine: {m_MagazineUsed.magazine != null}\n"

            );
            if (m_MagazineUsed.magazine)
            {
                if (m_CurrentMagazine.magazine)
                    MagazineRelease();
                else if (!m_MagazineInHand)
                    CreateMagazineInHand();
            }
        }
    }

        void Shoot()
        {
            if (!m_BarrelLocation || !m_BulletPrefab || (!m_CurrentMagazine.magazine && m_MagazineUsed.magazine)) return;
            //  GameObject bullet;
            //  bullet = Instantiate(bulletPrefab, barrelLocation.position, barrelLocation.rotation);
            // bullet.GetComponent<Rigidbody>().AddForce(barrelLocation.forward * shotPower);

            GameObject tempFlash;
            Instantiate(m_BulletPrefab, m_BarrelLocation.position, m_BarrelLocation.rotation).GetComponent<Rigidbody>().AddForce(m_BarrelLocation.forward * m_ShotPower);
            if (m_MuzzleFlashPrefab)
                tempFlash = Instantiate(m_MuzzleFlashPrefab, m_BarrelLocation.position, m_BarrelLocation.rotation);

            VibrationManager.BuzzNode(m_ShootVibrationDuration, m_shootVibrationForce, m_Grabbable.grabbedWithController.m_Controller);
            // Destroy(tempFlash, 0.5f);
            //  Instantiate(casingPrefab, casingExitLocation.position, casingExitLocation.rotation).GetComponent<Rigidbody>().AddForce(casingExitLocation.right * 100f);

        }

        void CasingRelease()
        {
            if (!m_CasingPrefab) return;
            GameObject casing;
            casing = Instantiate(m_CasingPrefab, m_CasingExitLocation.position, m_CasingExitLocation.rotation) as GameObject;
            casing.GetComponent<Rigidbody>().AddExplosionForce(550f, (m_CasingExitLocation.position - m_CasingExitLocation.right * 0.3f - m_CasingExitLocation.up * 0.6f), 1f);
            casing.GetComponent<Rigidbody>().AddTorque(new Vector3(0, Random.Range(100f, 500f), Random.Range(10f, 1000f)), ForceMode.Impulse);

            Destroy(casing, 10f);
        }

        void MagazineRelease()
        {
            if(m_Animator && animationForMagazineHolder)
            {
            m_Animator.SetBool("MagazineHolderOpen", true);
            }
            GameObject mag = m_CurrentMagazine.magazine.gameObject;
            mag.transform.SetParent(null);
            VibrationManager.BuzzNode(0.3f, VibrationForce.LIGHT, m_Grabbable.grabbedWithController.m_Controller);
            mag.GetComponent<Rigidbody>().isKinematic = false;
            m_CurrentMagazine.magazine = null;

            Destroy(mag, 10f);
        }

        void CreateMagazineInHand()
        {
            GameObject newMagazine = (GameObject)Instantiate(m_MagazineUsed.magazine.gameObject);
            WeaponMagazine weaponMagazine = newMagazine.GetComponent<WeaponMagazine>();
            WeaponGrabber grabberToGiveMagazineTo = m_Grabbable.grabbedWithController == InputHandler.instance.RightHand.GetComponent<WeaponGrabber>() ? InputHandler.instance.LeftHand.GetComponent<WeaponGrabber>() : InputHandler.instance.RightHand.GetComponent<WeaponGrabber>();
            m_MagazineInHand = weaponMagazine;
            weaponMagazine.AttachToHand(grabberToGiveMagazineTo.m_gripTransform);
        }

        public void AttachMagazine(WeaponMagazine magazine)
        {

            m_MagazineInHand = null;

            m_CurrentMagazine.magazine = magazine;
            if (m_Animator && animationForMagazineHolder)
            {
                m_Animator.SetBool("MagazineHolderOpen", false);
            }

        }

        public void ActivateHandItem()
        {

        }

        public void DeactivateHandItem()
        {
            Destroy(m_MagazineInHand);
            m_MagazineInHand = null;
            if (m_Animator && animationForMagazineHolder)
            {
                m_Animator.SetBool("MagazineHolderOpen", false);
            }
        }

    
}

[System.Serializable]
public class Magazine
{
    public WeaponMagazine magazine;
    public int capacity = 8;
    public int bulletsInMagazine = 8;
        
}
