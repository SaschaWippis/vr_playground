﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
public class VibrationManager : MonoBehaviour
{
    

    public static void BuzzBothHands(float seconds, VibrationForce vibrationForce)
    {
        BuzzNode(seconds, vibrationForce, XRNode.LeftHand);
        BuzzNode(seconds, vibrationForce, XRNode.RightHand);
    }

    public static void BuzzNode(float seconds,VibrationForce vibrationForce, OVRInput.Controller controller)
    {
        XRNode node = XRNode.LeftHand;
        if (controller == OVRInput.Controller.LTouch)
        {
            node = XRNode.LeftHand;
        }
        else if (controller == OVRInput.Controller.RTouch)
        {
            node = XRNode.RightHand;
        }
        BuzzNode(seconds, vibrationForce, node);
    }
    
    public static void BuzzNode(float seconds, VibrationForce vibrationForce, XRNode node)
    {
        HapticCapabilities caps = new HapticCapabilities();

        if (!InputDevices.GetDeviceAtXRNode(node).TryGetHapticCapabilities(out caps))
            return;

        if (caps.supportsImpulse)
        {
            float force = .5f;
            switch (vibrationForce)
            {
                case VibrationForce.LIGHT:
                    force = .1f;
                    break;
                case VibrationForce.MEDIUM:
                    force = .5f;
                    break;
                case VibrationForce.HARD:
                    force = 1f;
                    break;

            }
            InputDevices.GetDeviceAtXRNode(node).SendHapticImpulse(0, force, seconds);
        }
        else if (caps.supportsBuffer)
        {
            byte[] clip = { };
            if (GenerateBuzzClip(seconds, node, ref clip))
            {
                InputDevices.GetDeviceAtXRNode(node).SendHapticBuffer(0, clip);
            }
        }
    }

    public static bool GenerateBuzzClip(float seconds, XRNode node, ref byte[] clip)
    {
        HapticCapabilities caps = new HapticCapabilities();

        if (!InputDevices.GetDeviceAtXRNode(node).TryGetHapticCapabilities(out caps))
            return false;

        int clipCount = (int)(caps.bufferFrequencyHz * seconds);
        clip = new byte[clipCount];
        for (int i = 0; i < clipCount; i++)
        {
            clip[i] = byte.MaxValue;
        }

        return true;
    }
  

}
