﻿using UnityEngine;
public interface IPickupable
{
    void Pickup(Transform grabber);
    void Drop();
    GameObject getGameObject();
}