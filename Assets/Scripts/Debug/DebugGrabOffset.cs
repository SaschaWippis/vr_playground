﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class DebugGrabOffset : MonoBehaviour
{
    public static DebugGrabOffset instance;

    bool inMenu;
    Text logText;
    Text sliderTextX, sliderTextY, sliderTextZ, sliderTextRotX, sliderTextRotY, sliderTextRotZ,logTextNormal;
    Text magazineText;
    Text sliderFlyForceText;
    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        var sliderX = DebugUIBuilder.instance.AddSlider("X -Left<>Right", -0.1f, 0.1f, onXChanged);
        var sliderTextsX = sliderX.GetComponentsInChildren<Text>();
        sliderTextsX[0].text = "X - Left<>Right";
        sliderTextX = sliderTextsX[1];

        var sliderY = DebugUIBuilder.instance.AddSlider("Y - Down<>Up", -0.1f, 0.1f, onYChanged);
        var sliderTextsY = sliderY.GetComponentsInChildren<Text>();
        sliderTextsY[0].text = "Y - Down<>Up";
        sliderTextY = sliderTextsY[1];

        var sliderZ = DebugUIBuilder.instance.AddSlider("Z - Back<>Forward", -0.1f, 0.1f, onZChanged);
        var sliderTextsZ = sliderZ.GetComponentsInChildren<Text>();
        sliderTextsZ[0].text = "Z - Back<>Forward";
        sliderTextZ = sliderTextsZ[1];

        var sliderRotX = DebugUIBuilder.instance.AddSlider("RotX", -90f, 90f, onRotXChanged);
        var sliderTextsRotX = sliderRotX.GetComponentsInChildren<Text>();
        sliderTextsRotX[0].text = "RotX";
        sliderTextRotX = sliderTextsRotX[1];

        var sliderRotY = DebugUIBuilder.instance.AddSlider("RotY", -90f, 90f, onRotYChanged);
        var sliderTextsRotY = sliderRotY.GetComponentsInChildren<Text>();
        sliderTextsRotY[0].text = "RotY";
        sliderTextRotY = sliderTextsRotY[1];

        var sliderRotZ = DebugUIBuilder.instance.AddSlider("RotZ", -90f, 90f, onRotZChanged);
        var sliderTextsRotZ = sliderRotZ.GetComponentsInChildren<Text>();
        sliderTextsRotZ[0].text = "RotZ";
        sliderTextRotZ = sliderTextsRotZ[1];

        var log = DebugUIBuilder.instance.AddLabel("Offset");
        logText = log.GetComponent<Text>();

        var magText = DebugUIBuilder.instance.AddLabel("Magazine");
        magazineText = log.GetComponent<Text>();

        var logs = DebugUIBuilder.instance.AddLabel("Log",1);
        logTextNormal = logs.GetComponent<Text>();


        var sliderFlyForce = DebugUIBuilder.instance.AddSlider("Flyforce", 0f, 50f, onFlyForceChanged,false,1);
        var sliderFlyForceTexts = sliderFlyForce.GetComponentsInChildren<Text>();
        sliderFlyForceTexts[0].text = "Flyforce";
        sliderFlyForceText = sliderFlyForceTexts[1];
    }

    void Update()
    {

        if (OVRInput.GetDown(OVRInput.Button.Three))
        {
            
            if (inMenu) DebugUIBuilder.instance.Hide();
            else
            {
                DebugUIBuilder.instance.Show();
                sliderTextX.text = string.Format("{0:00.00000}", GetCurrentGrabbable().m_GrabbedPosition.x);
                sliderTextY.text = string.Format("{0:00.00000}", GetCurrentGrabbable().m_GrabbedPosition.y);
                sliderTextZ.text = string.Format("{0:00.00000}", GetCurrentGrabbable().m_GrabbedPosition.z);
            }
            inMenu = !inMenu;
        }
    }
    private void LateUpdate()
    {
    }

    private void onFlyForceChanged(float f)
    {
        sliderFlyForceText.text = f.ToString();
        HandThruster[] thrusters = FindObjectsOfType<HandThruster>();
        foreach (HandThruster thruster in thrusters) thruster.m_ThrustForce = f;
    }

    private void onXChanged(float f)
    {
        if (GetCurrentGrabbable())
        {
            Vector3 offset = GetCurrentGrabbable().m_GrabbedPosition;
            offset.x = f;
            sliderTextX.text = string.Format("{0:00.00000}", f);
            GetCurrentGrabbable().m_GrabbedPosition = offset;
            LogOffset(offset);
        }
    }

    private void onYChanged(float f)
    {
        if (GetCurrentGrabbable())
        {
            Vector3 offset = GetCurrentGrabbable().m_GrabbedPosition;
            offset.y = f;
            sliderTextY.text = string.Format("{0:00.00000}", f);
            GetCurrentGrabbable().m_GrabbedPosition = offset;
            LogOffset(offset);
        }
    }

    private void onZChanged(float f)
    {
        if (GetCurrentGrabbable())
        {
            Vector3 offset = GetCurrentGrabbable().m_GrabbedPosition;
            offset.z = f;
            sliderTextZ.text = string.Format("{0:00.00000}", f);
            GetCurrentGrabbable().m_GrabbedPosition = offset;
            LogOffset(offset);
        }
    }
    private void onRotXChanged(float f)
    {
        if (GetCurrentGrabbable())
        {
            Vector3 rot = GetCurrentGrabbable().transform.localEulerAngles;
            rot.x = f;
            sliderTextRotX.text = string.Format("{0:0.00}", f);
            GetCurrentGrabbable().transform.localEulerAngles = rot;

        }
    }
    private void onRotYChanged(float f)
    {
        if (GetCurrentGrabbable())
        {
            Vector3 rot = GetCurrentGrabbable().transform.localEulerAngles;
            rot.y = f;
            sliderTextRotY.text = string.Format("{0:0.00}", f);
            GetCurrentGrabbable().transform.localEulerAngles = rot;

        }
    }
    private void onRotZChanged(float f)
    {
        if (GetCurrentGrabbable())
        {
            Vector3 rot = GetCurrentGrabbable().transform.localEulerAngles;
            rot.z = f;
            sliderTextRotZ.text = string.Format("{0:0.00}", f);
            GetCurrentGrabbable().transform.localEulerAngles = rot;

        }
    }

    public void LogOffset(Vector3 offset)
    {
        logText.text = $"X: {offset.x}\nY:{offset.y}\nZ:{offset.z}";
    }
    public void Log(string log)
    {
        logText.text = log;
    }
    public void SetMagazineText(string txt)
    {
        magazineText.text = txt;
    }

    public void LogExternal(string txt)
    {
        logTextNormal.text = txt;
    }

    WeaponGrabbable GetCurrentGrabbable()
    {
        WeaponGrabber[] grabbers = FindObjectsOfType<WeaponGrabber>();
        foreach(WeaponGrabber grabber in grabbers)
        {
            if(grabber.getCurrentlyGrabbed() && grabber.getCurrentlyGrabbed().GetComponent<WeaponGrabbable>())
            {
               return grabber.getCurrentlyGrabbed().GetComponent<WeaponGrabbable>();
            }
        }
        return null;
    }
   
    
}
