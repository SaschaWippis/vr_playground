﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugHands : MonoBehaviour
{
    public static DebugHands instance;

    bool inMenu;

    Text logText1, logText2;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        var log1 = DebugUIBuilder.instance.AddLabel("Log");
        logText1 = log1.GetComponent<Text>();

        var log2 = DebugUIBuilder.instance.AddLabel("LogSecondary");
        logText2 = log2.GetComponent<Text>();

       

    }
   
    void Update()
    {
        if (OVRInput.GetDown(OVRInput.Button.Four))
        {
            if (inMenu) DebugUIBuilder.instance.Hide();
            else DebugUIBuilder.instance.Show();
            inMenu = !inMenu;
        }
    }

    public void Log(string message)
    {
        logText1.text = message;
        logText1.color = Color.white;
    }
    public void Log(string message, Color color)
    {
        logText1.text = message;
        logText1.color = color;
    }
    public void LogSecondary(string message)
    {
        logText2.text = message;
        logText2.color = Color.white;
    }
    public void LogSecondary(string message, Color color)
    {
        logText2.text = message;
        logText2.color = color;
    }
}
