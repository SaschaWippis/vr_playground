﻿
public interface IHandItem
{
    void ActivateHandItem();
    void DeactivateHandItem();
    
}