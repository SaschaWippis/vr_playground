﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponMagazine : MonoBehaviour
{
    public bool attachedToWeapon;
    public bool attachedToHand;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (attachedToHand)
        {
            Collider[] overlappingColliders = Physics.OverlapSphere(transform.position, .1f);
            foreach(Collider coll in overlappingColliders)
            {
                if (coll.GetComponentInParent<Gun>().m_magazineHolderCollider == coll)
                {
                    AttachToWeapon(coll.GetComponentInParent<Gun>());
                    return;
                }
            }
        }
        
    }

    public void AttachToHand(Transform hand)
    {
        transform.SetParent(hand);
        transform.position = hand.position;
        transform.localEulerAngles = Vector3.zero;
        GetComponent<Rigidbody>().isKinematic = true;
        attachedToHand = true;
    }

    public void AttachToWeapon(Gun gun)
    {
        
        transform.SetParent(gun.m_MagazineHolder);
        transform.localPosition = Vector3.zero;
        transform.localEulerAngles = Vector3.zero;
        GetComponent<Rigidbody>().isKinematic = true;
        attachedToHand = false;
        attachedToWeapon = true;
        gun.AttachMagazine(this);
        
    }
}
