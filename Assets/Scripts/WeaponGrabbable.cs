﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Handgun
//rotationRight 12.69 3.36 1.05
//rotationLeft 0 0 0
//position -0.00028 0.03188 -0.02296

//Bow
//rotationRight 0.18 -15.43 0
//rotationLeft 0 0 0
//position 0.00235 0.08625 -0.02439

//HandCannon
//rotationRight 0 0 0
//rotationLeft 0 0 0
//position 0 0 -0.01617

public class WeaponGrabbable : MonoBehaviour,IPickupable
{
    public Collider[] m_CollidersToDisable;
    public bool m_KeepAttached;
    public Vector3 m_GrabbedRotationRight;
    [Tooltip("If Vector3.zero, the Grabbed Rotation Right is used on both hands")]
    public Vector3 m_GrabbedRotationLeft;
    public Vector3 m_GrabbedPosition;

    private Rigidbody m_Rigidbody;
    
    bool m_PickedUp;
    public WeaponGrabber grabbedWithController;

    private void Awake()
    {
        m_Rigidbody = GetComponent<Rigidbody>();

    }

    public void Pickup(Transform grabber)
    {
        WeaponGrabber weaponGrabber = grabber.GetComponent<WeaponGrabber>();
        grabbedWithController = weaponGrabber;
        Transform gripTransform = weaponGrabber.m_gripTransform;
        m_Rigidbody.isKinematic = true;
        m_Rigidbody.useGravity = false;
        m_PickedUp = true;
        transform.SetParent(gripTransform);

        Quaternion grabbedQuaternion = Quaternion.Euler(m_GrabbedRotationRight);
        if(weaponGrabber.m_Controller == OVRInput.Controller.LTouch)
        {
            m_GrabbedPosition.x = -m_GrabbedPosition.x;
            if (m_GrabbedRotationLeft != Vector3.zero)
                grabbedQuaternion = Quaternion.Euler(m_GrabbedRotationLeft);
        }
        transform.localRotation = grabbedQuaternion;
        transform.localPosition = m_GrabbedPosition;

        if (m_CollidersToDisable.Length != 0)
            foreach(Collider c in m_CollidersToDisable) c.enabled = false;

        if (GetComponent<IHandItem>() != null)
            GetComponent<IHandItem>().ActivateHandItem();
        if (GetComponentInChildren<IHandItem>() != null)
            GetComponentInChildren<IHandItem>().ActivateHandItem();
    }

    private void Update()
    {
        if (!grabbedWithController) return;
        transform.localPosition = m_GrabbedPosition;
    }

    public void Drop()
    {
        
        transform.SetParent(null);
        m_Rigidbody.isKinematic = false;
        m_Rigidbody.useGravity = true;
        m_Rigidbody.velocity = OVRInput.GetLocalControllerVelocity(grabbedWithController.m_Controller);
        m_PickedUp = false;
        grabbedWithController = null;
        if (m_CollidersToDisable.Length != 0)
            foreach (Collider c in m_CollidersToDisable) c.enabled = true;

        if (GetComponent<IHandItem>() != null)
            GetComponent<IHandItem>().DeactivateHandItem();
        if (GetComponentInChildren<IHandItem>() != null)
            GetComponentInChildren<IHandItem>().DeactivateHandItem();
    }
    public GameObject getGameObject()
    {
        return gameObject;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(transform.position + m_GrabbedPosition,0.01f);
    }
}
