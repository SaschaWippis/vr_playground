﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyFinishedParticles : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        ParticleSystem psys = this.GetComponent<ParticleSystem>();
        Destroy(this.gameObject, psys.main.duration);
    }
    
}
