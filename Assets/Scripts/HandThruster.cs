﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandThruster : MonoBehaviour,IHandItem
{
    bool activated;
    public GameObject m_ThrustEffect;
    WeaponGrabbable m_weaponGrabbable;
    public float m_ThrustForce = 10;
    RigidbodyPlayerController m_AttachedRigidbodyController;
    OVRCameraRig m_CameraRig;
    void Awake()
    {
        m_weaponGrabbable = GetComponent<WeaponGrabbable>();
        m_AttachedRigidbodyController = FindObjectOfType<RigidbodyPlayerController>();
        m_CameraRig = FindObjectOfType<OVRCameraRig>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!activated) { return; }
        float thrust = OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, m_weaponGrabbable.grabbedWithController.m_Controller);
    
        if(m_AttachedRigidbodyController != null)
            m_AttachedRigidbodyController = m_weaponGrabbable.grabbedWithController.m_RigidbodyPlayerController;
        if (m_CameraRig == null && m_AttachedRigidbodyController != null)
            m_CameraRig = FindObjectOfType<OVRCameraRig>();


        if (thrust > 0.01f)
        {
            if (m_ThrustEffect)
            {
                if (!m_ThrustEffect.activeSelf)
                    m_ThrustEffect.SetActive(true);
                m_ThrustEffect.transform.localScale = Vector3.one * (thrust * .25f);
            }
            if (m_AttachedRigidbodyController != null && m_CameraRig != null)
            {
                Vector3 thrustDirection = -transform.up;
                
                m_AttachedRigidbodyController.Fly(thrustDirection, thrust * m_ThrustForce);
                return;
            }
            
        }
        else
        {
           
            if (m_ThrustEffect.activeSelf)
                m_ThrustEffect.SetActive(false);
        }
         }

    public void ActivateHandItem()
    {
        activated = true;
        
    }

    public void DeactivateHandItem()
    {
        activated = false;
    }
}
