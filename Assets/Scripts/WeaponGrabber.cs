﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponGrabber : MonoBehaviour
{

    public OVRInput.Controller m_Controller;
    public Transform m_gripTransform;

    private Rigidbody m_Rigidbody;
    WeaponGrabbable m_currentGrabbedObject;
    public OVRPlayerController m_OVRPlayerController;
    public RigidbodyPlayerController m_RigidbodyPlayerController;

    private void Awake()
    {
        m_Rigidbody = GetComponent<Rigidbody>();

        if (!m_RigidbodyPlayerController)
            Debug.LogError("No RigidbodyPlayerController attached to WeaponGrabber");
    }

    private void Update()
    {
        
        
        if (OVRInput.GetDown(OVRInput.Button.PrimaryHandTrigger, m_Controller) && !m_currentGrabbedObject && getGrabbableWeapon())
        {
            if(!getGrabbableWeapon().grabbedWithController)
                TryPickup();
        }
        if(OVRInput.GetUp(OVRInput.Button.PrimaryHandTrigger, m_Controller) && m_currentGrabbedObject && !m_currentGrabbedObject.m_KeepAttached)
        {
                TryDrop();
        }
        if(OVRInput.GetUp(OVRInput.Button.PrimaryThumbstick,m_Controller) && m_currentGrabbedObject && m_currentGrabbedObject.m_KeepAttached)
        {
            TryDrop();
        }
    }

    public WeaponGrabbable getCurrentlyGrabbed()
    {
        return m_currentGrabbedObject;
    }

    WeaponGrabbable getGrabbableWeapon()
    {
        Collider[] overlappingColliders = Physics.OverlapSphere(m_gripTransform.position, 0.05f);
        foreach (Collider collider in overlappingColliders)
        {
            WeaponGrabbable grabbable = collider.GetComponent<WeaponGrabbable>();
            if (grabbable == null) grabbable = collider.GetComponentInParent<WeaponGrabbable>();
            if (grabbable) return grabbable;
        }
        return null;
    }

    public void TryPickup()
    {
        Collider[] overlappingColliders = Physics.OverlapSphere(m_gripTransform.position, 0.05f);
        foreach(Collider collider in overlappingColliders)
        {
            IPickupable pickupable = collider.GetComponent<IPickupable>();
            if (pickupable == null) pickupable = collider.GetComponentInParent<IPickupable>();
            if (pickupable != null)
            {
                WeaponGrabbable grabbable = pickupable.getGameObject().GetComponent<WeaponGrabbable>();
                
                    pickupable.Pickup(this.transform);
                    m_currentGrabbedObject = grabbable;
                
                break;
            }
        }
    }

    void TryDrop()
    {
        if (m_currentGrabbedObject)
        {
            m_currentGrabbedObject.GetComponent<IPickupable>().Drop();
            m_currentGrabbedObject = null;
        }
    }
}
