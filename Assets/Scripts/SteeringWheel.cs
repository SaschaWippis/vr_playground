﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteeringWheel : MonoBehaviour
{

    public Transform attachedHand;
    public Vector3 maxRadius;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.GetComponent<WeaponGrabber>())
        {
            attachedHand = other.transform;
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.transform.GetComponent<WeaponGrabber>())
        {
            MoveSphere(other.transform.position);
            OrientSphere(other.transform.localRotation * other.transform.GetComponentInParent<RigidbodyPlayerController>().transform.rotation);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.transform.GetComponent<WeaponGrabber>())
        {
            attachedHand = null;
        }
    }


    void MoveSphere(Vector3 handPosition)
    {
        Vector3 clampedPos = new Vector3(
            Mathf.Clamp(handPosition.x, transform.parent.position.x - maxRadius.x, transform.parent.position.x + maxRadius.x),
            Mathf.Clamp(handPosition.y, transform.parent.position.y - maxRadius.y, transform.parent.position.y + maxRadius.y),
            Mathf.Clamp(handPosition.z, transform.parent.position.z - maxRadius.z, transform.parent.position.z + maxRadius.z)
            );
        transform.position = clampedPos;
    }

    void OrientSphere(Quaternion rotation)
    {
        transform.rotation = rotation;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireCube(transform.parent.position, maxRadius);
    }
}
