﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemInHand : MonoBehaviour,IHandItem
{
    public enum ItemType
    {
        NONE,
        INSTANTIATE
    }

    public ItemType itemType;
    bool activated;
    [Header("ItemType: NONE")]
    public GameObject[] m_Items;

    [Header("ItemType: INSTANTIATE")]
    public Transform m_Controller;
    public GameObject m_instantiatePrefab;
    public OVRInput.Button instantiateButton;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!activated) return;
        if (OVRInput.GetDown(instantiateButton))
        {
            GameObject prefab = Instantiate(m_instantiatePrefab);
                prefab.transform.position = m_Controller.position;
        }
    }

    public void ActivateHandItem()
    {
        activated = true;
        switch (itemType)
        {
            case ItemType.NONE:
                foreach (GameObject obj in m_Items)
                    obj.SetActive(true);
                break;
        }
        
    }

    public void DeactivateHandItem()
    {
        activated = false;
        switch (itemType)
        {
            case ItemType.NONE:
                foreach (GameObject obj in m_Items)
                    obj.SetActive(false);
                break;
        }
    }
}
