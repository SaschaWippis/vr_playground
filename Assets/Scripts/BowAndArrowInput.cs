﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OVR;
using System;

public class BowAndArrowInput : MonoBehaviour,IHandItem
{
    bool activated;

    public Bow m_Bow = null;
    public GameObject m_OppositeController = null;
    public Camera m_CenterEye;
    public OVRInput.Controller m_Controller = OVRInput.Controller.None;
    public Arrow m_ArrowInHand;

    private void Awake()
    {
        if (!m_Bow) m_Bow = GetComponent<Bow>();
        m_CenterEye = Camera.main;
    }

    private void Update()
    {
        if (!activated) return;

        if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger, m_Controller))
            m_Bow.Pull(m_OppositeController.transform);

        if (OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger, m_Controller))
            m_Bow.Release();

        OVRPose headPose = OVRPose.identity;
        Vector3 pullHand = OVRInput.GetLocalControllerPosition(m_Controller);
        DebugGrabOffset.instance.LogExternal("R: " + pullHand.ToString());
        if (HandOverRightShoulder())
        {
            if (!m_ArrowInHand && !m_Bow.getCurrentArrow())
            {
                CreateArrow();
            }
            
        }

        if (m_ArrowInHand && !m_Bow.getCurrentArrow())
        {
            if (Vector3.Distance(m_Bow.transform.position, m_OppositeController.transform.position) < .14f)
            {
                Destroy(m_ArrowInHand.gameObject);
                m_ArrowInHand = null;
                StartCoroutine(m_Bow.CreateArrow(.1f));
            }
            
        }
    }
    
    void CreateArrow()
    {
        GameObject arrow = Instantiate(m_Bow.m_ArrowPrefab, m_OppositeController.transform);
        arrow.transform.localPosition = new Vector3(0, 0, .35f);
        arrow.transform.localEulerAngles = Vector3.zero;
        m_ArrowInHand = arrow.GetComponent<Arrow>();
        VibrationManager.BuzzNode(.4f, VibrationForce.LIGHT, m_Controller);
    }

    bool HandOverRightShoulder()
    {
        Vector3 rightHand = m_OppositeController.transform.position;
        if (rightHand.y > m_CenterEye.transform.position.y && 
            rightHand.x > m_CenterEye.transform.position.x && 
            rightHand.z -0.02f < m_CenterEye.transform.position.z )
            return true;

        return false;
    }

    public void ActivateHandItem()
    {
        activated = true;
    }

    public void DeactivateHandItem()
    {
        activated = false;
        m_Bow.ResetBow();
        if(m_ArrowInHand)Destroy(m_ArrowInHand.gameObject);
        m_ArrowInHand = null;
    }
}
