﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
public class RigidbodyPlayerController : MonoBehaviour
{
    public bool isDebug;
    [Space(20)]
    public float m_MoveSpeed = 1f;
    /// <summary>
    /// The rate acceleration during movement.
    /// </summary>
    public float Acceleration = 0.1f;

    /// <summary>
    /// The rate of rotation when using a gamepad.
    /// </summary>
    public float RotationAmount = 1.5f;

    /// <summary>
    /// The rate of rotation when using the keyboard.
    /// </summary>
    public float RotationRatchet = 45.0f;

    /// <summary>
    /// The player will rotate in fixed steps if Snap Rotation is enabled.
    /// </summary>
    [Tooltip("The player will rotate in fixed steps if Snap Rotation is enabled.")]
    public bool SnapRotation = true;

    /// <summary>
    /// If true, tracking data from a child OVRCameraRig will update the direction of movement.
    /// </summary>
    public bool HmdRotatesY = true;
    /// <summary>
    /// If true, reset the initial yaw of the player controller when the Hmd pose is recentered.
    /// </summary>
    public bool HmdResetsY = true;
    /// <summary>
    /// The CameraHeight is the actual height of the HMD and can be used to adjust the height of the character controller, which will affect the
    /// ability of the character to move into areas with a low ceiling.
    /// </summary>
    [NonSerialized]
    public float CameraHeight;

    /// <summary>
    /// When true, user input will be applied to rotation. Set this to false whenever the player controller needs to ignore input for rotation.
    /// </summary>
    public bool EnableRotation = true;

    /// <summary>
    /// Rotation defaults to secondary thumbstick. You can allow either here. Note that this won't behave well if EnableLinearMovement is true.
    /// </summary>
    public bool RotationEitherThumbstick = false;

    /// <summary>
    /// Is the character touching the ground
    /// </summary>
    bool m_Grounded;

    /// <summary>
    /// Velocity of the rigidbody of the previous frame
    /// </summary>
    Vector3 m_PreviousVelocity;

    [Header("Assets")]
    public GameObject m_GroundSmashParticlePrefab;

    [Space(20)]

    private float MoveScale = 1.0f;
    private float MoveScaleMultiplier = 1.0f;
    private Rigidbody m_Rigidbody;
    private OVRCameraRig m_CameraRig = null;
    private CapsuleCollider m_CapsuleCollider = null;
    public event Action<Transform> TransformUpdated;
    public float InitialYRotation { get; private set; }
    private OVRPose? InitialPose;
    private bool prevHatLeft = false;
    private bool prevHatRight = false;
    private float SimulationRate = 90f;
    private bool SkipMouseRotation = true;
    private bool ReadyToSnapTurn; // Set to true when a snap turn has occurred, code requires one frame of centered thumbstick to enable another snap turn.
    private float buttonRotation = 0f;
    private float RotationScaleMultiplier = 1.0f;
    [NonSerialized] // This doesn't need to be visible in the inspector.
    public bool Teleported;

    private Vector3 MoveThrottle;
    
    private void Awake()
    {
        m_Rigidbody = GetComponent<Rigidbody>();
        m_CameraRig = GetComponentInChildren<OVRCameraRig>();
        m_CapsuleCollider = GetComponent<CapsuleCollider>();

        InitialYRotation = transform.rotation.eulerAngles.y;
    }

    void Start()
    {
        // Add eye-depth as a camera offset from the player controller
        var p = m_CameraRig.transform.localPosition;
        p.z = OVRManager.profile.eyeDepth;
        m_CameraRig.transform.localPosition = p;
    }

   
    void Update()
    {
        if (!isGrounded()) m_Grounded = false;
        if (GetGroundHitVelocity() > 25)
        {
            Debug.Log("Ground hit with " + m_PreviousVelocity.y);
            VibrationManager.BuzzBothHands(1.5f, VibrationForce.HARD);
            GameObject groundSmashParticles = Instantiate(m_GroundSmashParticlePrefab);
            groundSmashParticles.transform.position = new Vector3(
                transform.position.x + m_CapsuleCollider.center.x,
                transform.position.y + m_CapsuleCollider.center.y - m_CapsuleCollider.bounds.extents.y,
                transform.position.z + m_CapsuleCollider.center.z
                );
        }
        ControlRigidbody();
        

        if (OVRManager.OVRManagerinitialized)
        {
            OVRManager.display.RecenteredPose += ResetOrientation;

            if (m_CameraRig != null)
            {
                m_CameraRig.UpdatedAnchors += UpdateTransform;
            }
        }
        else
            return;
    }

    void OnDisable()
    {
        if (OVRManager.OVRManagerinitialized)
        {
            OVRManager.display.RecenteredPose -= ResetOrientation;

            if (m_CameraRig != null)
            {
                m_CameraRig.UpdatedAnchors -= UpdateTransform;
            }
        }
        
    }

    public void UpdateTransform(OVRCameraRig rig)
    {
        Transform root = m_CameraRig.trackingSpace;
        Transform centerEye = m_CameraRig.centerEyeAnchor;

        if (HmdRotatesY && !Teleported)
        {
            Vector3 prevPos = root.position;
            Quaternion prevRot = root.rotation;

            transform.rotation = Quaternion.Euler(0.0f, centerEye.rotation.eulerAngles.y, 0.0f);

            root.position = prevPos;
            root.rotation = prevRot;
        }
        //TODO: Call movement function here
        UpdateCameraRig();
        if (TransformUpdated != null)
        {
            TransformUpdated(root);
        }

        UpdateRotation();
    }

    void UpdateRotation()
    {
        if (EnableRotation)
        {
            Vector3 euler = transform.rotation.eulerAngles;
            float rotateInfluence = SimulationRate * Time.deltaTime * RotationAmount * RotationScaleMultiplier;

            bool curHatLeft = OVRInput.Get(OVRInput.Button.PrimaryShoulder);

            if (curHatLeft && !prevHatLeft)
                euler.y -= RotationRatchet;

            prevHatLeft = curHatLeft;

            bool curHatRight = OVRInput.Get(OVRInput.Button.SecondaryShoulder);

            if (curHatRight && !prevHatRight)
                euler.y += RotationRatchet;

            prevHatRight = curHatRight;

            euler.y += buttonRotation;
            buttonRotation = 0f;


#if !UNITY_ANDROID || UNITY_EDITOR
            if (!SkipMouseRotation)
                euler.y += Input.GetAxis("Mouse X") * rotateInfluence * 3.25f;
#endif

            if (SnapRotation)
            {
                if (OVRInput.Get(OVRInput.Button.SecondaryThumbstickLeft) ||
                    (RotationEitherThumbstick && OVRInput.Get(OVRInput.Button.PrimaryThumbstickLeft)))
                {
                    if (ReadyToSnapTurn)
                    {
                        euler.y -= RotationRatchet;
                        ReadyToSnapTurn = false;
                    }
                }
                else if (OVRInput.Get(OVRInput.Button.SecondaryThumbstickRight) ||
                    (RotationEitherThumbstick && OVRInput.Get(OVRInput.Button.PrimaryThumbstickRight)))
                {
                    if (ReadyToSnapTurn)
                    {
                        euler.y += RotationRatchet;
                        ReadyToSnapTurn = false;
                    }
                }
                else
                {
                    ReadyToSnapTurn = true;
                }
            }
            else
            {
                Vector2 secondaryAxis = OVRInput.Get(OVRInput.Axis2D.SecondaryThumbstick);
                if (RotationEitherThumbstick)
                {
                    Vector2 altSecondaryAxis = OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick);
                    if (secondaryAxis.sqrMagnitude < altSecondaryAxis.sqrMagnitude)
                    {
                        secondaryAxis = altSecondaryAxis;
                    }
                }
                euler.y += secondaryAxis.x * rotateInfluence;
            }

            transform.rotation = Quaternion.Euler(euler);
        }
    }

    public void UpdateCameraRig()
    {
        if (InitialPose == null)
        {
            // Save the initial pose so it can be recovered if useProfileData
            // is turned off later.
            InitialPose = new OVRPose()
            {
                position = m_CameraRig.transform.localPosition,
                orientation = m_CameraRig.transform.localRotation
            };
        }
        var p = m_CameraRig.transform.localPosition;
        if (OVRManager.instance.trackingOriginType == OVRManager.TrackingOrigin.EyeLevel)
        {
            p.y = OVRManager.profile.eyeHeight - (0.5f * m_CapsuleCollider.height) + m_CapsuleCollider.center.y;
        }
        else if (OVRManager.instance.trackingOriginType == OVRManager.TrackingOrigin.FloorLevel)
        {
            p.y = -(0.5f * m_CapsuleCollider.height) + m_CapsuleCollider.center.y;
        }
        m_CameraRig.transform.localPosition = p;
    }

    /// <summary>
	/// Resets the player look rotation when the device orientation is reset.
	/// </summary>
	public void ResetOrientation()
    {
        if (HmdResetsY && !HmdRotatesY)
        {
            Vector3 euler = transform.rotation.eulerAngles;
            euler.y = InitialYRotation;
            transform.rotation = Quaternion.Euler(euler);
        }
    }

    public void Fly(Vector3 direction, float force)
    {
        m_Rigidbody.AddForce(direction * force, ForceMode.Acceleration);

    }
    public void Fly(Vector3 direction, float force,Vector3 position)
    {
        m_Rigidbody.AddForceAtPosition(direction * force, position, ForceMode.Acceleration);

    }

    void ControlRigidbody()
    {
        m_PreviousVelocity = m_Rigidbody.velocity;
        float moveInfluence = Acceleration * 0.1f * MoveScale * MoveScaleMultiplier;
        Vector2 primaryAxis =OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick);
        if (isDebug) primaryAxis = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        
        Quaternion ort = transform.rotation;
        Vector3 ortEuler = ort.eulerAngles;
        ortEuler.z = ortEuler.x = 0f;
        ort = Quaternion.Euler(ortEuler);
        if (primaryAxis.y > 0.01f)
            MoveThrottle +=  transform.forward * primaryAxis.y;

        if (primaryAxis.y < -0.01f)
            MoveThrottle+=  transform.forward * primaryAxis.y;

        if (primaryAxis.x < 0.01f)
            MoveThrottle +=  transform.right * primaryAxis.x;

        if (primaryAxis.x > -0.01f)
            MoveThrottle += transform.right * primaryAxis.x;

        if (MoveThrottle != Vector3.zero && isGrounded())
        {
            m_Rigidbody.velocity = MoveThrottle;
            
        }
        MoveThrottle = Vector3.zero;

        if(transform.position.y < 0)
        {
            m_Rigidbody.AddForce(Vector3.up * (9.51f*-transform.position.y), ForceMode.Acceleration);
        }
        
    }

    bool isGrounded()
    {
        float distToGround = m_CapsuleCollider.bounds.extents.y;
        bool grounded = Physics.Raycast(transform.position, -transform.up, distToGround + 0.1f);
        
        return grounded;
    }

    float GetGroundHitVelocity()
    {
        if(m_PreviousVelocity.y < -2f && isGrounded() && !m_Grounded)
        {
            m_Grounded = true;
            
            return -m_PreviousVelocity.y;
        }

            
        return 0;
    }

    
}
