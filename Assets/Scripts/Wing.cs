﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wing : MonoBehaviour
{
    [Header("Attach to")]
    public Transform m_Hand;
    public Transform m_Head;
    [Header("Wing parts")]
    public Transform m_WingHand;
    public Transform m_WingHead;

    public Vector3 up;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        m_WingHand.position = m_Hand.position;
        m_WingHead.position = m_Head.position;

        Vector3 rel = m_Hand.position - m_WingHead.position;
        m_WingHead.rotation = Quaternion.LookRotation(rel,up);

    }
}
