﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveWithTransform : MonoBehaviour
{
    public Transform moveWithTransform;
    public bool moveOnX;
    public bool moveOnY;
    public bool moveOnZ;

    // Update is called once per frame
    void Update()
    {
        if (!moveWithTransform) return;

        Vector3 pos = transform.position;

        pos.x = moveOnX ? moveWithTransform.position.x : transform.position.x;
        pos.y = moveOnY ? moveWithTransform.position.y : transform.position.y;
        pos.z = moveOnZ ? moveWithTransform.position.z : transform.position.z;

        transform.position = pos;
    }
}
